"""Routes tests."""
from typing import List, Tuple

import pytest
from aiohttp.web import Application
from pytest_aiohttp.plugin import AiohttpClient


@pytest.mark.asyncio
async def test_percolate(
    app: Application,
    aiohttp_client: AiohttpClient,
    query_topics: Tuple[str, List[str]],
) -> None:
    """Tests API method 'percolate'."""
    client = await aiohttp_client(app)
    query, topics = query_topics

    resp = await client.get('/api/percolate', params={'doc': query})
    body = await resp.json()

    assert resp.status == 200
    assert topics == body['response']['topics']
