"""Web app."""
from aiohttp import web
from aiohttp_swagger import setup_swagger

from . import routes

mosapp = web.Application()
mosapp.router.add_route('GET', '/api/percolate', routes.percolate)

setup_swagger(mosapp, swagger_url='/api/doc', ui_version=2)
