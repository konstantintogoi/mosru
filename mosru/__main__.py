"""CLI."""
import logging

import click
import yaml


@click.group()
@click.option(
    '--config',
    type=click.File(),
    help='Path to a config file.',
)
@click.pass_context
def cli(ctx: click.Context, config: click.File) -> None:
    """App CLI."""
    ctx.ensure_object(dict)
    ctx.obj['config'] = yaml.safe_load(config)
    logging.basicConfig(level=logging.INFO)


@cli.command(help='Start service.')
@click.pass_context
def start(ctx: click.Context) -> None:
    """Запуск сервера.

    Args:
        ctx (Context): контекст

    """
    from aiohttp import web
    from elasticsearch import Elasticsearch
    from mosru import mosapp

    click.echo('Starting ..')
    conn = ctx.obj['config']['es']['connection']
    mosapp['elasticsearch'] = Elasticsearch(**conn)
    web.run_app(mosapp)


@cli.command(help='Populate percolator with queries.')
@click.argument('queries', type=click.Path(exists=True))
@click.pass_context
def populate(ctx: click.Context, queries: str) -> None:
    """Загрузка тестовых данных.

    Args:
        ctx (Context): контекст
        queries (str): запросы

    """
    from mosru import elastic

    conn = ctx.obj['config']['es']['connection']
    click.secho('Populating ..', fg='blue')
    click.secho(f'ElasticSearch on: {conn}', fg='blue')
    elastic.populate(conn, queries)
    click.secho('Populating succeeded', fg='green')


def main() -> click.Context:
    """Главная функция.

    Returns:
        Context

    """
    return cli(obj={})


main()
