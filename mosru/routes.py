"""API эндпоинты."""
from aiohttp import web
from elasticsearch import Elasticsearch
from loguru import logger as log


async def percolate(request: web.Request) -> web.Response:  # noqa: WPS210
    """Percolates a document through queries.

    Args:
        request (Request): http запрос

    Returns:
        response (Response): http ответ

    ---
    summary: Процедить запрос
    description: Возвращает темы, которые содержатся в тексте запроса
    produces:
    - application/json
    parameters:
    - in: query
      name: doc
      description: запрос, который нужно "процедить"
      required: true
      schema:
        type: string
    responses:
        "200":
            description: success
        "500":
            description: error

    """
    doc: str = request.rel_url.query['doc']
    es: Elasticsearch = request.app['elasticsearch']

    try:
        res = es.search(body={  # type: ignore
            'query': {
                'percolate': {
                    'field': 'query',
                    'document': {'doc': doc},
                },
            },
        })
    except Exception as exc:
        log.exception(exc)
        response = {
            'error': {
                'error_msg': repr(exc),
                'request_params': {'doc': doc},
            },
        }
        status = 500
    else:
        hits = res['hits']['hits']
        response = {
            'response': {
                'topics': [hit['_index'] for hit in hits],
            },
        }
        status = 200

    return web.json_response(response, status=status)
