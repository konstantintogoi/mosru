"""API клиент."""
from typing import Any, Dict

from aiohttp.client_exceptions import ClientError
from httpx import AsyncClient, Response
from loguru import logger as log


class Client:
    """Асинхронный API клиент."""

    error_code = 500

    def __init__(self, base_url: str) -> None:
        """Инициализация клиента."""  # noqa: DAR101
        self.client = AsyncClient(follow_redirects=True, base_url=base_url)

    async def __aenter__(self) -> 'Client':
        """Entering async context."""  # noqa: DAR201
        return self

    async def __aexit__(self, *exc) -> None:
        """Exit async context."""  # noqa: DAR101
        await self.client.aclose()

    def __getattr__(self, path: str) -> 'APIEndpoint':
        """Возвращает эндпоинт."""  # noqa: DAR101, DAR201
        return APIEndpoint(self, path)

    async def request(
        self,
        coro: Any,
        path: str,
        **kwargs: Any,
    ) -> Any:
        """Запрос к API.

        Args:
            coro: асинхронный метод get/post
            path: путь, который надо добавить к API_URL
            kwargs: kwargs

        Returns:
            Response

        Raises:
            ClientError: при исключениях или когда код ответа неуспешный

        """
        try:
            resp: Response = await coro(path, **kwargs)
        except Exception as req_exc:
            detail = 'request {0} {1} {2} failed due to error {3}'.format(
                self.client.base_url, path, kwargs, req_exc,
            )
            log.error(detail)
            raise ClientError(self.error_code, detail)
        else:
            detail = '{0} {1} {2}'.format(
                resp.request.method,
                resp.url,
                resp.status_code,
            )
            log.info(detail)

        resp.raise_for_status()

        try:
            return resp.json()
        except Exception as json_exc:
            resp_data = resp.read().decode()
            detail = f'request failed: {resp.url}, {kwargs}.'
            detail = f'{detail} Invalid response: "{resp_data}". {json_exc}'
            log.error(detail)
            raise ClientError(self.error_code, detail)


class APIEndpoint:
    """API Endpoint."""

    def __init__(self, api: Client, path: str) -> None:
        """Инициализация для по определенному пути."""  # noqa: DAR101
        self.api = api
        self.path = path

    def __call__(self, path: str) -> 'APIEndpoint':
        """Поддержка цепочки путей динамически."""  # noqa: DAR101, DAR201
        self.path = f'{self.path}/{path}'
        return self

    def __getattr__(self, path: str) -> 'APIEndpoint':
        """Поддержка цепочки путей через точку."""  # noqa: DAR101, DAR201
        self.path = f'{self.path}/{path}'
        return self

    async def get(self, **kwargs: Dict[str, object]) -> Dict[str, object]:
        """GET запрос."""  # noqa: DAR101, DAR201
        return await self.api.request(
            self.api.client.get, self.path, params=kwargs,
        )

    async def post(self, json: Any) -> Any:
        """POST запрос."""  # noqa: DAR101, DAR201
        return await self.api.request(
            self.api.client.post, self.path, json=json,
        )
